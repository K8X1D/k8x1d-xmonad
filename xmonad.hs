import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.Ungrab

import XMonad.Layout.Magnifier
import XMonad.Layout.ThreeColumns

import XMonad.Hooks.EwmhDesktops

-- Tags
import XMonad.Actions.TagWindows
import XMonad.Prompt    -- to use tagPrompt
import XMonad.Prompt.Shell
import XMonad.Prompt.Pass

import XMonad.Util.ClickableWorkspaces
import XMonad.Hooks.EwmhDesktops 
  

--import Graphics.X11.ExtraTypes.XF86

-- Gaps
import XMonad.Layout.Gaps
import XMonad.Layout.Spacing

-- Scratchpad
import XMonad.Util.Scratchpad


-- Startup hook
import XMonad.Util.SpawnOnce


main :: IO ()
main = xmonad
     . xmobarProp
     . withEasySB (statusBarProp "xmobar" (pure myXmobarPP)) defToggleStrutsKey
     $ myConfig

  
myConfig = def
    { modMask = mod4Mask  -- Rebind Mod to the Super key
    , terminal = myTerminal  -- Terminal
    , layoutHook = myLayout      -- Use custom layouts
    , startupHook = startup  -- Program to launch at start 
    , manageHook = myManageHook  -- Match on certain windows
    , XMonad.workspaces = ["web", "code", "mail", "music", "social", "other"]
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor
    , borderWidth = 2
    }
  `additionalKeysP` myKeys

myTerminal = "st"
myNormalBorderColor = "#282828"
myFocusedBorderColor = "#282828"
--myFocusedBorderColor = "#ebdbb2"

myManageHook :: ManageHook
myManageHook = composeAll
    [ moveC "firefox-default" "web"
    , moveC "Nyxt" "web"
    , moveC "icecat-default" "web"
    , moveC "Brave-browser" "web"
    , moveC "Chromium-browser" "web"
    , moveC "Emacs" "code"
    , moveC "LunarVim" "code"
    , moveC "icedove-default" "mail"
    , moveC "Mattermost" "mail"
    , moveC "Music Player" "music"
    , moveC "Signal" "social"
    , moveC "Caprine" "social"
    , moveC "discord" "social"

    , scratchpadManageHookDefault

    , className =? "Gimp" --> doFloat
    , className =? "Alacritty" --> doCenterFloat 
    , className =? "st-256color" --> doCenterFloat 
    , isDialog            --> doFloat
    ]
   where moveC c w = className =? c --> doShift w

--myLayout = gaps [(U,10), (D,10), (R,10), (L,10)] $ tiled ||| Mirror tiled ||| Full ||| threeCol
myLayout =  spacing 2 $ tiled ||| Mirror tiled ||| Full ||| threeCol
  where
    threeCol = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    tiled    = Tall nmaster delta ratio
    nmaster  = 1      -- Default number of windows in the master pane
    ratio    = 1/2    -- Default proportion of screen occupied by master pane
    delta    = 3/100  -- Percent of screen to increment by when resizing panes

-- Programs to start 
startup :: X ()
startup = do
          -- user services
          spawnOnce "shepherd"
          -- status bar
          spawnOnce "xmobar"
          -- keyboard config
          spawnOnce "xset r rate 300 50"
          -- touchpad config
          spawnOnce "xinput set-prop 'PNP0C50:00 06CB:CD79 Touchpad' 'libinput Tapping Enabled' 1"
          -- dpms config
          -- spawn "xset dpms 300 0 0" == problem without hibernate when locked, test without dpms
          spawnOnce "xset s off -dpms"
          -- fix initial brightness
          spawnOnce "brightnessctl s 1"
          -- Automatic lockscreen
          spawnOnce "Locker='slock && sleep 1'"
          spawnOnce "xss-lock --transfer-sleep-lock -- $Locker --nofork"
          -- Proton decryption
          spawnOnce "protonmail-bridge"
          -- Wallpapers slideshow
          spawnOnce "random_wallpaper.sh"
          -- Proof reading (local)
          spawnOnce "languagetool-preconfigured-server"
          -- Notification
          spawnOnce "dunst"
          -- Battery level notification
          spawnOnce "batsignal -w 15 -c 5 -d2 -m 5"
          -- Color temperature adjustment
          spawnOnce "redshift -l '45.50884:-73.58781' -t '6500:3000'"
          -- Compositor
          spawnOnce "picom"
          -- Dropbox
          spawnOnce "dropbox"

myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = magenta " • "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
    , ppHidden          = white . wrap " " ""
    , ppHiddenNoWindows = lowWhite . wrap " " ""
    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    "[") (white    "]") . red . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow

    -- | Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    blue, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#ff79c6" ""
    blue     = xmobarColor "#bd93f9" ""
    white    = xmobarColor "#f8f8f2" ""
    yellow   = xmobarColor "#f1fa8c" ""
    red      = xmobarColor "#ff5555" ""
    lowWhite = xmobarColor "#bbbbbb" ""


myShellPromptConfig :: XPConfig
myShellPromptConfig = def { bgColor  = "#ebdbb2"
                    , fgColor  = "#282828"
                    , borderColor = "#ebdbb2"
                    , font = "xft:Julia Mono:pixelsize=18"
                    , height = 35
                    , position = Top
          }
  
myKeys =  
    [ ("M-<Escape>", spawn "slock")
    , ("M-e"  , spawn "emacs")
    -- , ("M-f"  , spawn "firefox")
    , ("M-i"  , spawn "icedove")
    , ("M-m"  , spawn "st -c 'Music Player' -e ncmpcpp")
    -- menu
    --, ("M-p"  , spawn "dmenu_run -b -fn 'JetBrains Mono:size=12' -nb '#282828' -nf '#ebdbb2' -sb '#ebdbb2' -sf '#282828'")
  , ("M-p", shellPrompt myShellPromptConfig)
  --, ("M-d", passPrompt def) -- don't work...
    -- Volume Conctrol
    , ("<XF86AudioMute>", spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    , ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
    , ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
    -- Brightness control
    , ("<XF86MonBrightnessUp>", spawn "brightnessctl set +2.5%")
    , ("<XF86MonBrightnessDown>", spawn "brightnessctl set 2.5%-")
    -- Tag
    , ("M-C-a", tagPrompt def (\s -> withFocused (addTag s)))
    , ("M-C-h", tagPrompt def (\s -> withTaggedGlobalP s shiftHere))  -- shift windows with user defined tags (input) here
    , ("M-C-d", tagDelPrompt def)  -- delete a user-defined tag (input)
    , ("M-C-t", tagPrompt def  focusUpTaggedGlobal)  -- switch between windows in all workspaces with user-defined tags (input)
    , ("M-C-f", tagPrompt def (`withTaggedGlobal` float)) -- float windows with user-defined tags (input)
    -- Scratchpad 
    , ("M-o", scratchpadSpawnActionTerminal myTerminal)
    
  ]
